import {
  SCROLL_LOCK,
  SCROLL_UNLOCK,
  SCROLL_LOCK_FREEZE,
  SCROLL_LOCK_UNFREEZE,
  ScrollLock,
  ScrollUnlock,
  ScrollLockFreeze,
  ScrollLockUnfreeze
} from "./types";

export const scrollLock = (): ScrollLock => ({
  type: SCROLL_LOCK,
});

export const scrollUnlock = (): ScrollUnlock => ({
  type: SCROLL_UNLOCK,
});

export const scrollLockFreeze = (): ScrollLockFreeze => ({
  type: SCROLL_LOCK_FREEZE,
});

export const scrollLockUnfreeze = (): ScrollLockUnfreeze => ({
  type: SCROLL_LOCK_UNFREEZE,
});