export const SCROLL_LOCK = "SCROLL_LOCK";
export const SCROLL_UNLOCK = "SCROLL_UNLOCK";
export const SCROLL_LOCK_FREEZE = "SCROLL_LOCK_FREEZE";
export const SCROLL_LOCK_UNFREEZE = "SCROLL_LOCK_UNFREEZE";

export interface ScrollLock {
  type: typeof SCROLL_LOCK;
}

export interface ScrollUnlock {
  type: typeof SCROLL_UNLOCK;
}

export interface ScrollLockFreeze {
  type: typeof SCROLL_LOCK_FREEZE;
}

export interface ScrollLockUnfreeze {
  type: typeof SCROLL_LOCK_UNFREEZE;
}