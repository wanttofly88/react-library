import {
  SLIDER_ADD,
  SLIDER_REMOVE,
  SLIDER_UPDATE,
  SLIDER_NEXT,
  SLIDER_PREV,
  SLIDER_TO,
  SLIDER_SET_DIRECTION,
  SliderAdd,
  SliderRemove,
  SliderUpdate,
  SliderTo,
  SliderPrev,
  SliderNext,
  SliderSetDirection
} from "./types";

export const sliderAdd = (
  id: string,
  total: number,
  index: number,
  continuous: boolean,
  direction: number,
): SliderAdd => ({
  type: SLIDER_ADD,
  payload: {
    id: id,
    total: total,
    index: index,
    continuous: continuous,
    direction: direction
  },
});

export const sliderUpdate = (
  id: string,
  total: number,
  index: number,
  continuous: boolean,
  direction: number
): SliderUpdate => ({
  type: SLIDER_UPDATE,
  payload: {
    id: id,
    total: total,
    index: index,
    continuous: continuous,
    direction: direction
  },
});

export const sliderRemove = (id: string): SliderRemove => ({
  type: SLIDER_REMOVE,
  payload: {
    id: id,
  },
});

export const sliderNext = (id: string): SliderNext => ({
  type: SLIDER_NEXT,
  payload: {
    id: id,
  },
});

export const sliderPrev = (id: string): SliderPrev => ({
  type: SLIDER_PREV,
  payload: {
    id: id,
  },
});

export const sliderTo = (id: string, index: number, direction: number): SliderTo => ({
  type: SLIDER_TO,
  payload: {
    id: id,
    index: index,
    direction: direction
  },
});

export const sliderSetDirection = (id: string, direction: number): SliderSetDirection => ({
  type: SLIDER_SET_DIRECTION,
  payload: {
    id: id,
    direction: direction
  },
});
