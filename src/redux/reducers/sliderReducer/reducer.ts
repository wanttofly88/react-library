import { Reducer } from "react";
import type { AnyAction } from "redux";
import {
  SLIDER_ADD,
  SLIDER_REMOVE,
  SLIDER_UPDATE,
  SLIDER_NEXT,
  SLIDER_PREV,
  SLIDER_TO,
  SLIDER_SET_DIRECTION
} from "./types";

interface SliderItemI {
  id: string;
  index: number;
  total: number;
  continuous: boolean;
  direction: number;
}

interface SliderStateI {
  items: Array<SliderItemI>;
}

const initialState: SliderStateI | undefined = {
  items: [],
};

export const sliderReducer: Reducer<SliderStateI | undefined, AnyAction> = (
  state = initialState,
  action
) => {
  switch (action.type) {
    case SLIDER_ADD:
      return {
        ...state,
        items: [...state.items, action.payload as SliderItemI],
      };

    case SLIDER_REMOVE:
      return {
        ...state,
        items: state.items.filter((item) => item.id !== action.payload.id),
      };

    case SLIDER_UPDATE:
      return {
        ...state,
        items: state.items.find((item) => item.id === action.payload.id)
          ? state.items.map((item) => {
              if (item.id === action.payload.id) {
                return action.payload as SliderItemI;
              } else {
                return item;
              }
            })
          : [...state.items, action.payload as SliderItemI],
      };

    case SLIDER_NEXT:
      return {
        ...state,
        items: state.items.map((item) => {
          if (item.id === action.payload.id) {
            return {
              ...item,
              index:
                item.index + 1 <= item.total - 1
                  ? item.index + 1
                  : item.continuous
                  ? 0
                  : item.total - 1,
              direction: 1,
            };
          } else {
            return item;
          }
        }),
      };

    case SLIDER_PREV:
      return {
        ...state,
        items: state.items.map((item) => {
          if (item.id === action.payload.id) {
            return {
              ...item,
              index:
                item.index - 1 >= 0
                  ? item.index - 1
                  : item.continuous
                  ? item.total - 1
                  : 0,
              direction: -1
            };
          } else {
            return item;
          }
        }),
      };

    case SLIDER_TO:
      return {
        ...state,
        items: state.items.map((item) => {
          if (item.id === action.payload.id) {
            return {
              ...item,
              index:
                action.payload.index >= 0
                  ? action.payload.index <= item.total - 1
                    ? action.payload.index
                    : item.index
                  : item.index,
              direction: action.payload.direction
            };
          } else {
            return item;
          }
        }),
      };
    
    case SLIDER_SET_DIRECTION:
      return {
        ...state,
        items: state.items.map((item) => {
          if (item.id === action.payload.id) {
            return {
              ...item,
              direction: action.payload.direction
            };
          } else {
            return item;
          }
        }),
      };

    default:
      return state;
  }
};
