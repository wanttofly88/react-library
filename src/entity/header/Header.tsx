import styles from "./Header.module.scss";
import { ModalControl } from "../../shared/modal/ModalControl";
import { CSSProperties } from "react";
import { useScrollLock } from "../../shared/scroll/useScrollLock";

export const Header = () => {
  const scrollLock = useScrollLock();

  const style: CSSProperties = {
    right: scrollLock.offset,
  };

  return (
    <header className={styles.Header} style={style}>
      <div className={`${styles.Wrapper} ${styles.Wrapper_FullWidth}`}>
        <ModalControl
          id="menu"
          alwaysFocusable={true}
          className={styles.MenuButton}
          activeClassName={styles.MenuButton_Active}
          mode="toggle-all"
        >
          <div
            className={`${styles.MenuButton__Line} ${styles.MenuButton__InactiveTop}`}
          ></div>
          <div
            className={`${styles.MenuButton__Line} ${styles.MenuButton__InactiveBottom}`}
          ></div>
          <div
            className={`${styles.MenuButton__Line} ${styles.MenuButton__ActiveTop}`}
          ></div>
          <div
            className={`${styles.MenuButton__Line} ${styles.MenuButton__ActiveBottom}`}
          ></div>
        </ModalControl>
      </div>
    </header>
  );
};
