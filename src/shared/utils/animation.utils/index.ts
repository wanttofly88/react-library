import { simpleTween, SimpleTween } from "./simpleTween";
import { TweenComposer } from "./TweenComposer";
import { transition, Transition } from "./transition";
import { createLoop } from "./createLoop";
import fps from "./fps";

export { simpleTween, TweenComposer, transition, createLoop, fps };

export type { SimpleTween, Transition };
