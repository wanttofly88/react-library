import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux/store";
import {
  modalOpen,
  modalClose,
  modalCloseAll,
  modalToggle,
  modalToggleAll
} from "../../redux/reducers/modalReducer/actions";
import { useAlwaysFocusable } from "../focus/useAlwaysFocusable";
import { modalControlDefaultProps } from "./defaults";
import { DEFAULT_GROUP_ID } from "./types";
import { useCallback, useRef } from "react";

export type ControlActions = "toggle" | "open" | "close" | "close-all" | "toggle-all";

export interface ModalControlProps {
  id: string;
  mode?: ControlActions;
  className?: string;
  groupId?: string;
  style?: React.CSSProperties;
  children?: React.ReactElement[] | string | null;
  activeClassName?: string;
  alwaysFocusable?: boolean;
}

export const ModalControl = ({
  id,
  mode = modalControlDefaultProps.mode,
  className,
  groupId = DEFAULT_GROUP_ID,
  style,
  children,
  activeClassName = modalControlDefaultProps.activeClassName,
  alwaysFocusable = modalControlDefaultProps.alwaysFocusable,
}: ModalControlProps) => {
  const dispatch = useDispatch();
  const modalState = useSelector((state: RootState) => state.modal?.groups[groupId]);
  const wrapperElement = useRef<HTMLButtonElement | null>(null);

  useAlwaysFocusable(alwaysFocusable ? wrapperElement : undefined);

  const handleClick = useCallback(() => {
    if (mode === "open") {
      dispatch(modalOpen(id, groupId));
    } else if (mode === "close") {
      dispatch(modalClose(id, groupId));
    } else if (mode === "toggle") {
      dispatch(modalToggle(id, groupId));
    } else if (mode === "close-all") {
      dispatch(modalCloseAll(groupId));
    } else if (mode === "toggle-all") {
      dispatch(modalToggleAll(id, groupId));
    }
  }, [dispatch, id, groupId, mode]);

  const classes:string[] = [];
  const active = modalState && modalState.active.includes(id);

  if (mode === "open" || mode === "close" || mode === "toggle") {
    if (modalState && modalState.active.includes(id)) {
      classes.push(activeClassName);
    }
  } else if (mode === "close-all" || mode === "toggle-all") {
    if (modalState && modalState.active.length) {
      classes.push(activeClassName);
    }
  }

  if (className) {
    classes.push(className);
  }

  return (
    <button ref={wrapperElement} className={classes.join(" ")} onClick={handleClick} style={style} aria-haspopup="true" aria-expanded={active}>
      {children}
    </button>
  );
};
