import React, { useEffect } from "react";
import { RootState } from "../../redux/store";
import { useDispatch, useSelector } from "react-redux";
import { modalGroupDefaultProps } from "./defaults";
import { modalGroupSet } from "../../redux/reducers/modalReducer/actions";
import { Modes } from "./types";
import { useScrollLock } from "../scroll/useScrollLock";
import { OffsetScrollbar } from "../scroll/types";

export interface ModalGroupProps {
  id?: string;
  mode?: Modes;
  scrollLock?: boolean;
  focusLock?: boolean;
  offsetScrollbar?: OffsetScrollbar;
  children: JSX.Element | JSX.Element[];
  className?: string;
}

export const ModalGroup = ({
  id = modalGroupDefaultProps.id,
  mode = modalGroupDefaultProps.mode,
  scrollLock = modalGroupDefaultProps.scrollLock,
  focusLock = modalGroupDefaultProps.scrollLock,
  offsetScrollbar = modalGroupDefaultProps.offsetScrollbar,
  className,
  children,
}: ModalGroupProps) => {
  const dispatch = useDispatch();

  const modalState = useSelector((state: RootState) =>
    state.modal ? state.modal.groups[id] : undefined
  );

  const lockScroll:boolean = scrollLock && !!modalState?.active.length;
  const scrollLocker = useScrollLock();

  useEffect(() => {
    dispatch(modalGroupSet(id, mode, scrollLock, focusLock));
    return () => { 
    }
  }, [dispatch, focusLock, id, mode, scrollLock]);

  useEffect(() => {
    if (lockScroll) {
      scrollLocker.lock();
    } else {
      scrollLocker.unlock();
    }
  }, [lockScroll, dispatch, scrollLocker]);

  const childrenWithProps = React.Children.map(children, (child) => {
    if (React.isValidElement(child) && typeof child.type === "function") {
      return React.cloneElement(child, {
        ...(child as React.ReactElement).props,
        groupId: id,
        focusLock: focusLock,
        offsetScrollbar: offsetScrollbar
      });
    } else {
      return child;
    }
  });

  return <div className={className}>{childrenWithProps}</div>;
};
