import React from "react";
import { defaultAnimation } from "./defaultAnimation";

export const DEFAULT_ANIMATION = "DEFAULT_ANIMATION";

export interface UseAnimationReturn {
    visible: boolean
}

export interface UseAnimation {
    (id: string, groupId: string, wrapperElement:React.MutableRefObject<null | HTMLElement>): UseAnimationReturn
}

export const animationTypes = {
    [ defaultAnimation.ANIMATION_NAME ]: defaultAnimation
}
