import { useEffect, useLayoutEffect, useRef, useState } from "react";
import { useSelector } from "react-redux";
import { RootState } from "../../../../redux/store";
import { UseAnimation } from "./types";
import { transition } from "../../../utils/animation.utils";

export const ANIMATION_NAME = "DEFAULT_ANIMATION";
export const APPEAR_DURATION = 0.45;
export const HIDE_DURATION = 0.45;

export const useAnimation: UseAnimation = (id, groupId, wrapperElement) => {
  const modalState = useSelector((state: RootState) =>
    state.modal ? state.modal.groups[groupId] : undefined
  );
  const [visible, setVisible] = useState(false);
  const timeout: React.MutableRefObject<null | ReturnType<typeof setTimeout>> = useRef(null);

  const stateActive = modalState?.active.includes(id);

  useEffect(() => {
    if (stateActive) {
      if (timeout.current) {
        clearTimeout(timeout.current);
      }

      setVisible(true);
    } else {
      timeout.current = setTimeout(() => {
        setVisible(false);
      }, HIDE_DURATION * 1000);
    }
  }, [stateActive]);

  useLayoutEffect(() => {
    if (!wrapperElement.current) {
      return;
    }

    if (stateActive && visible) {
      transition(wrapperElement.current, APPEAR_DURATION, {
        opacity: 1,
      }, {
        ease: "default",
        skipFrame: true
      });
    } else if (!stateActive) {
      transition(wrapperElement.current, HIDE_DURATION, {
        opacity: 0,
      }, {
        ease: "default"
      });
    }
  }, [stateActive, visible, wrapperElement]);

  return {
    visible,
  };
};

export const defaultAnimation = {
  ANIMATION_NAME,
  APPEAR_DURATION,
  HIDE_DURATION,
  useAnimation,
};
