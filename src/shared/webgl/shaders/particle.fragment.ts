export default function() {
	return `
		void main() {
			vec4 color = vec4(1.0);
		    gl_FragColor = color;
		}
	`
};
