import { randomChunk } from "./chunks/random.fragment.chunk";

export default function () {
	return `
		attribute float size;

		void main() {
			vec4 mvPosition = modelViewMatrix * vec4( position, 1.0 );

			gl_PointSize = size / -mvPosition.z;
			gl_Position = projectionMatrix * mvPosition;
		}
	`;
}