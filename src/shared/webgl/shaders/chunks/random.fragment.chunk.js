export const randomChunk = () => {
	return`
		highp float random(vec2 coords) {
			const highp float a = 12.9898, b = 78.233, c = 43758.5453;
			highp float dt = dot( coords.xy, vec2( a,b ) ), sn = mod( dt, M_PI );
			return fract(sin(sn) * c);
		}

		highp float random(float n) {
		    return fract(sin(n)*43758.5453);
		}
	`
};