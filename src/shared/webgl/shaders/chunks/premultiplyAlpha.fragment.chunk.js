export const premultiplyAlphaChunk = (options) => {
	options = options || {};
	let background = options.background || "vec3(0.0)";
	return `
		gl_FragColor.rgb = mix(${background}, gl_FragColor.rgb, gl_FragColor.a);
	`;
}