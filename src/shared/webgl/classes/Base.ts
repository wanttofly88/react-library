// import { FunctionArray } from "../../utils/array.utils.js";
// import { Vector3Array } from "../../utils/array.utils.js";

import { Vector3Array } from "../../utils/array.utils";

const defaultOptions = {
  position: { x: 0, y: 0, z: 0 },
  scale: { x: 1, y: 1, z: 1 },
  rotation: { x: 0, y: 0, z: 0 },
};

interface ConstructorOptions {
  parent: any,
  options: any,
  decorators: Array<any>
}

export interface Base {
  positionArray: Vector3Array;
  rotationArray: Vector3Array;
  scaleArray: Vector3Array;
}

export class Base {
  public parent;
  public camera;
  public parentObject;
  // private debug: boolean;

  constructor(constructorOptions: ConstructorOptions) {
    // this.checkIfLoad = this.checkIfLoad.bind(this);
    // this.checkIfActive = this.checkIfActive.bind(this);
    // this.handleResize = this.handleResize.bind(this);
    // this.updateUniforms = this.updateUniforms.bind(this);
    // this.updatePosition = this.updatePosition.bind(this);
    // this.loop = this.loop.bind(this);

    this.parent = constructorOptions.parent;

    if (this.parent.camera) {
      this.camera = this.parent.camera;
    }

    this.parentObject = this.parent.group
      ? this.parent.group
      : this.parent.scene
      ? this.parent.scene
      : null;

    // this.options = Object.assign(
    //   {},
    //   defaultOptions,
    //   constructorOptions.options
    // );
    // this.childDecorators = constructorOptions.decorators || [];

    // this.debug = this.parent.debug;

    // this.shortName = this.options.shortName;
    // this.namePrefix = this.options.namePrefix;
    // this.options.name = this.options.namePrefix + "-" + this.shortName;
    // this.name = this.options.name;

    // this.renderer = this.parent.renderer;

    // this.active = false;
    // this.loaded = false;

    // this.loopArray = new FunctionArray();
    // this.resizeArray = new FunctionArray();
    // this.activePromises = [];

    // this.activeFlags = new FunctionArray();
    // this.loadFlags = new FunctionArray();

    // if (this.parent instanceof HTMLElement) {
    //   this.component = this.parent;
    //   this.loadFlags.push(() => {
    //     return true;
    //   });
    // } else {
    //   this.component = this.parent.component;
    //   this.loadFlags.push(() => {
    //     return this.parent.loaded;
    //   });
    // }

    // this.activeFlags.push(() => {
    //   return this.loaded;
    // });

    // this.positionArray = new Vector3Array();
    // this.positionArray.push({
    //   name: "original",
    //   value: this.options.position,
    // });

    // this.rotationArray = new Vector3Array();
    // this.rotationArray.push({
    //   name: "original",
    //   value: this.options.rotation,
    // });

    // this.scaleArray = new Vector3Array();
    // this.scaleArray.push({
    //   name: "original",
    //   value: this.options.scale,
    // });

    // if (window.masterLoop) {
    //   this.loopMode = "master";
    // } else {
    //   this.loopMode = "independent";
    // }
  }

  // update(options) {
  //   this.options = Object.assign(this.options, options);

  //   this.positionArray.set({
  //     name: "original",
  //     value: this.options.position,
  //   });

  //   this.rotationArray.set({
  //     name: "original",
  //     value: this.options.rotation,
  //   });

  //   this.scaleArray.set({
  //     name: "original",
  //     value: this.options.scale,
  //   });

  //   this.onupdate();
  // }

  // async load() {
  //   this.updatePosition();
  //   this.handleResize();

  //   if (this.parent.loopArray) {
  //     this.parent.loopArray.push(this.loop);
  //   }
  //   if (this.parent.resizeArray) {
  //     this.parent.resizeArray.push(this.handleResize);
  //   }

  //   this.onload();
  // }

  // async unload() {
  //   this.updatePosition();
  //   this.handleResize();

  //   if (this.parent.loopArray) {
  //     this.parent.loopArray.remove(this.loop);
  //   }
  //   if (this.parent.resizeArray) {
  //     this.parent.resizeArray.remove(this.handleResize);
  //   }

  //   this.onunload();
  // }

  // async init() {
  //   this.oninit();

  //   await Promise.all(
  //     Object.values(this.childDecorators).map((d) =>
  //       d.attach(this, {
  //         debug: this.options.debug,
  //         defines: this.options.defines,
  //       })
  //     )
  //   );
  // }

  // async destroy() {
  //   if (this.unload) {
  //     this.unload();
  //   }

  //   if (this.meshes) {
  //     this.meshes.forEach((m) => {
  //       m.material.dispose();
  //       m.geometry.dispose();
  //     });
  //   }

  //   if (this.mesh) {
  //     this.mesh.material.dispose();
  //     this.mesh.geometry.dispose();
  //   }

  //   if (this.composer) {
  //     this.composer.dispose();
  //   }

  //   if (this.scene) {
  //     this.scene.dispose();
  //   }

  //   this.ondestroy();

  //   await Promise.all(
  //     Object.values(this.childDecorators).map((d) => d.detach(this, {}))
  //   );
  // }

  // activate() {}
  // deactivate() {}
  // oninit() {}
  // ondestroy() {}
  // onload() {}
  // onunload() {}
  // onloop() {}
  // onresize() {}
  // onupdate() {}

  // updateUniforms(uniforms) {
  //   for (let decoratorId in this.decorators) {
  //     if (this.decorators[decoratorId].updateUniforms) {
  //       this.decorators[decoratorId].updateUniforms(uniforms);
  //     }
  //   }
  //   if (this.meshes) {
  //     this.meshes.forEach(function (m) {
  //       for (let key in uniforms) {
  //         if (m.material.uniforms.hasOwnProperty(key)) {
  //           m.material.uniforms[key].value = uniforms[key];
  //         }
  //       }
  //     });
  //   }
  //   if (this.mesh) {
  //     for (let key in uniforms) {
  //       if (
  //         this.mesh.material.uniforms &&
  //         this.mesh.material.uniforms.hasOwnProperty(key)
  //       ) {
  //         this.mesh.material.uniforms[key].value = uniforms[key];
  //       }
  //     }
  //   }
  //   if (this.options.uniforms) {
  //     for (let key in uniforms) {
  //       if (this.options.uniforms.hasOwnProperty(key)) {
  //         this.options.uniforms[key].value = uniforms[key];
  //       }
  //     }
  //   }
  //   if (this.composer) {
  //     this.composer.updateUniforms(uniforms);
  //   }
  // }

  // updatePosition() {
  //   let object = this.group ? this.group : this.mesh ? this.mesh : null;

  //   if (!object) return;

  //   let position = this.positionArray.sum();
  //   if (
  //     Math.abs(position.x - object.position.x) > 0.01 ||
  //     Math.abs(position.y - object.position.y) > 0.01 ||
  //     Math.abs(position.z - object.position.z) > 0.01
  //   ) {
  //     // if (object.name.indexOf("mesh-letter") === -1) {
  //     //     console.log(object.name);
  //     // }
  //     object.position.set(position.x, position.y, position.z);
  //   }

  //   let rotation = this.rotationArray.sum();
  //   if (
  //     Math.abs(rotation.x - object.rotation.x) > 0.001 ||
  //     Math.abs(rotation.y - object.rotation.y) > 0.001 ||
  //     Math.abs(rotation.z - object.rotation.z) > 0.001
  //   ) {
  //     object.rotation.set(rotation.x, rotation.y, rotation.z);
  //   }

  //   let scale = this.scaleArray.multiply();
  //   if (
  //     Math.abs(scale.x - object.scale.x) > 0.001 ||
  //     Math.abs(scale.y - object.scale.y) > 0.001 ||
  //     Math.abs(scale.z - object.scale.z) > 0.001
  //   ) {
  //     object.scale.set(scale.x, scale.y, scale.z);
  //   }
  // }

  // async checkIfLoad() {
  //   let stateChanged = false;

  //   if (!this.loaded && this.loadFlags.boolReduce()) {
  //     this.loaded = true;
  //     stateChanged = true;
  //   } else if (this.loaded && !this.loadFlags.boolReduce()) {
  //     this.loaded = false;
  //     stateChanged = true;
  //   }

  //   try {
  //     await Promise.all(
  //       this.decorators
  //         ? Object.values(this.decorators).map((d) =>
  //             d.checkIfLoad ? d.checkIfLoad() : Promise.resolve()
  //           )
  //         : []
  //     );
  //     if (!stateChanged) {
  //       return Promise.resolve();
  //     }

  //     if (this.loaded) {
  //       if (this.load) {
  //         if (this.debug) {
  //           console.log("loaded " + this.name);
  //         }

  //         return this.load().then(this.checkIfActive);
  //       } else {
  //         return this.checkIfActive();
  //       }
  //     } else {
  //       if (this.unload) {
  //         if (this.debug) {
  //           console.log("unloaded " + this.name);
  //         }
  //         return this.unload().then(this.checkIfActive);
  //       } else {
  //         return this.checkIfActive;
  //       }
  //     }
  //   } catch (reject) {
  //     console.error(reject);
  //   }
  // }

  // cancelLoad() {
  //   this.activePromises.forEach(function (p) {
  //     p.canceled = true;
  //   });
  // }

  // async checkIfActive() {
  //   if (this.active) {
  //     if (!this.activeFlags.boolReduce()) {
  //       this.active = false;

  //       this.deactivate();

  //       if (this.debug) {
  //         console.log("inactive " + this.name);
  //       }
  //     }
  //   } else {
  //     if (this.activeFlags.boolReduce()) {
  //       this.active = true;

  //       this.activate();

  //       if (this.debug) {
  //         console.log("active " + this.name);
  //       }
  //     }
  //   }

  //   for (let decoratorId in this.decorators) {
  //     if (this.decorators[decoratorId].checkIfActive) {
  //       this.decorators[decoratorId].checkIfActive();
  //     }
  //   }
  // }

  // handleResize() {
  //   this.resizeArray.elements.forEach(function (resizeFunc) {
  //     if (typeof resizeFunc === "function") {
  //       resizeFunc();
  //     }
  //   });

  //   this.onresize();
  // }

  // loop(multiplier) {
  //   if (this.destruct) return;

  //   this.loopArray.elements.forEach(function (loopFunc) {
  //     if (typeof loopFunc === "function") {
  //       loopFunc(multiplier);
  //     }
  //   });

  //   this.onloop(multiplier);

  //   if (this.updatePosition) {
  //     this.updatePosition();
  //   }

  //   if (this.composer) {
  //     this.render();
  //   }
  // }

  // render() {
  //   if (this.loaded && this.active) {
  //     this.composer.render(null);
  //   }
  // }
}
