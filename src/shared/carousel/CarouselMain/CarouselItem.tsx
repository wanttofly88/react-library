import React, { useEffect, useRef, forwardRef } from "react";
import { getFocusable } from "../../utils/dom.utils";

interface CarouselItemProps {
  /**
   * If element is a fake, created to fill the empty space
   */
  fake: boolean;
  children: React.ReactElement | React.ReactElement[];
  className: string;
  style: React.CSSProperties;
}

export const CarouselItem = forwardRef<HTMLDivElement, CarouselItemProps>(
  ({ fake, children, className, style }: CarouselItemProps, ref) => {
    const wrapper = useRef<HTMLDivElement | null>(null);

    useEffect(() => {
      if (!wrapper.current) return;

      const focusable = getFocusable(wrapper.current);

      if (fake) {
        focusable.forEach((el) => {
          el.setAttribute(
            "tabindex",
            (parseInt(el.getAttribute("tabindex") || "0") - 1).toString()
          );
        });
      }

      return () => {
        if (fake) {
          focusable.forEach((el) => {
            el.setAttribute(
              "tabindex",
              Math.min(
                0,
                parseInt(el.getAttribute("tabindex") || "0") + 1
              ).toString()
            );
          });
        }
      };
    }, [fake, wrapper.current, children]);

    return (
      <div ref={ref} className={className} style={style}>
        {children}
      </div>
    );
  }
);
