import { carouselEventEmitter } from "..";

/**
 * Focus on interactive elements inside the carousel
 */
export interface TabControls {
  /**
   * Destructor
   */
  destroy: VoidFunction;
}

interface TabControlsOptions {
  container: HTMLElement;
  id: string;
  applySectionScrollFix: boolean;
  itemRefs: React.RefObject<HTMLDivElement>[];
}

export class TabControls {
  private container;
  private id;
  private itemRefs;
  private applySectionScrollFix;

  constructor({
    container,
    id,
    itemRefs,
    applySectionScrollFix
  }: TabControlsOptions) {
    this.container = container;
    this.id = id;
    this.itemRefs = itemRefs;
    this.applySectionScrollFix = applySectionScrollFix;

    document.addEventListener("keydown", this.handleKeyboard);
  }

  destroy = () => {
    document.removeEventListener("keydown", this.handleKeyboard);
  }

  handleKeyboard = (e: KeyboardEvent) => {
    if (e.key === "Tab") {
      setTimeout(() => {
        const activeElement = document.activeElement;

        if (!activeElement || !this.container.contains(activeElement)) {
          return;
        }

        this.itemRefs.forEach((item, i) => {
          if (item.current && item.current.contains(activeElement)) {
            carouselEventEmitter.dispatch({
              type: "carousel:toIndex",
              payload: {
                id: this.id,
                scrollValue: i,
              },
            });

            this.container.scrollTo(0, 0);

            requestAnimationFrame(() => {
              this.container.scrollTo(0, 0);
            });

            if (this.applySectionScrollFix) {
              const section = this.container.closest("section");
              if (section) {
                section.scrollTo(0, section.scrollTop);

                requestAnimationFrame(() => {
                  section.scrollTo(0, section.scrollTop);
                });
              }
            }
          }
        });
      }, 0);
    }
  };
}