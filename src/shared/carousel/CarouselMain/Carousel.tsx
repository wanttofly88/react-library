import React, {
  useCallback,
  useEffect,
  useState,
  useRef,
  CSSProperties,
} from "react";
import { syncResize } from "../../resize";
import { CarouselItem } from "./CarouselItem";
import { Resize, ResizeSimple, ResizeContinuous } from "./resize";
import { Motion, MotionCallback } from "./motion";
import { PointerControls, PointerCallback } from "./pointerControls";
import { simpleEase } from "../../utils/ease.utils";
import { EventEmitter, Event } from "../../utils/EventEmitter";
import { FocusControls } from "./focusControls";
import { TabControls } from "./tabControls";

// TODO: Align, Interactive auto, Composite animations
// WIP: Focus controls to check what current index is and move to next. On continuous scroll to closest, ruther then index

interface CarouselSizes {
  containerWidth: number;
  itemWidth: number;
  gapWidth: number;
  totalWidth: number;
  leftShift: number;
  numberOfFakeElements: number;
}

interface ElementsPerScreen {
  breakpoint: number;
  number: number;
}

interface PositionData {
  x: number;
  delta: number;
}

interface PositionsObject {
  [index: string]: PositionData;
}

interface Positions extends PositionsObject {
  motion: PositionData;
  modeShift: PositionData;
}

export interface CarouselProps {
  id: string;
  /**
   * Carousel mode. Should it be continuous or should it be limited by the wrapper.
   */
  mode?: "simple" | "continuous";
  /**
   * Carousel alignment.
   */
  align?: "left" | "center";
  /**
   * Carousel drag. If "auto", then carousel will be draggable only if its children width is higher then the wrapper width. Will be ignored if mode is set to continuous.
   */
  interactive?: "always" | "auto" | "never";
  /**
   * Carousel will resize it's elements according to elementMaxWidth and elementsPerScreen properties.
   */
  resizeElements?: boolean;
  /**
   * Gap between the elements in pixels
   */
  gap?: number;
  /**
   * Additional width outside of the container
   */
  padding?: number;
  /**
   * Outer padding measurment units
   */
  paddingUnits?: "px" | "vw" | "%";
  /**
   * Width of the elements in pixels
   */
  itemWidth?: number;
  /**
   * Maximum width of the elements in pixels
   */
  maximumItemWidth?: number;
  /**
   * Number of displayed elements per breakpoing. The syntax looks like this [{breakpoint: 320, number: 1}, {breakpoint: 640, number: 2}, {breakpoint: 1000, number:3}]
   */
  elementsPerScreen?: ElementsPerScreen[];
  /**
   * Maximum element width.
   */
  elementMaxWidth?: number;
  /**
   * Carousel will try to autoalign items with the wrapper.
   */
  inertionCorrection?: boolean;
  /**
   * Inertion decay factor
   */
  inertionDecay?: number;
  /**
   * Inertion interpolation function. Function that is used to calculate the inertion.
   */
  inertionInterpolationFunction?: (
    value: number,
    targetValue: number,
    inertionDecay: number
  ) => number;
  /**
   * Applies fix to the parent section. If user tabs through interactive elements inside of the carousel, browser may deside to scroll the closest "overflow:hidden" element, wich is ususally a section.
   */
  applySectionScrollFix?: boolean;

  className?: string;
  style?: CSSProperties;
  children: React.ReactElement[];
}

type CarouselEventTypes =
  | "carousel:to"
  | "carousel:toIndex"
  | "carousel:toPercent"
  | "carousel:toVw"
  | "carousel:by"
  | "carousel:byIndex"
  | "carousel:byPercent"
  | "carousel:byVw";

export interface CarouselEvent extends Event {
  type: CarouselEventTypes;
  payload: {
    id: string;
    scrollValue: number;
  };
}

export const carouselEventEmitter = new EventEmitter<CarouselEvent>();

export const Carousel = ({
  id,
  mode = "simple",
  inertionCorrection = true,
  align = "left",
  interactive = "always",
  gap = 30,
  padding = 0,
  paddingUnits = "px",
  itemWidth = 300,
  maximumItemWidth = 350,
  resizeElements = true,
  elementsPerScreen = [
    {
      breakpoint: 320,
      number: 1,
    },
    {
      breakpoint: 640,
      number: 2,
    },
    {
      breakpoint: 1024,
      number: 3,
    },
  ],
  inertionDecay = 16,
  inertionInterpolationFunction = simpleEase,
  applySectionScrollFix = true,
  className,
  style,
  children,
}: CarouselProps) => {
  /**
   * Carousel track element
   */
  const track = useRef<HTMLDivElement | null>(null);

  /**
   * Carousel html element
   */
  const wrapper = useRef<HTMLDivElement | null>(null);

  /**
   * Object with every value that can influence the position of the carosel track.
   */
  const carouselPosition = useRef<Positions>({
    motion: {
      x: 0,
      delta: 0,
    },
    modeShift: {
      x: 0,
      delta: 0,
    }
  });

  /**
   * Used in resize functon to align elements
   */
  const elementsPerScreenRef = useRef<ElementsPerScreen[]>([]);

  if (
    elementsPerScreenRef.current.length !== elementsPerScreen.length ||
    !elementsPerScreenRef.current.reduce((prev, current, index) => {
      return (
        prev &&
        current.breakpoint === elementsPerScreen[index].breakpoint &&
        current.number === elementsPerScreen[index].number
      );
    }, true)
  ) {
    elementsPerScreenRef.current = elementsPerScreen;
  }

  /**
   * Total number of children (except for the fake ones, created by carousel)
   */
  const total = children.length;

  const carouselSizesRef = useRef<CarouselSizes>({
    containerWidth: 0,
    itemWidth: 0,
    gapWidth: 0,
    totalWidth: 0,
    leftShift: 0,
    numberOfFakeElements: 0,
  });

  let itemRefs: React.RefObject<HTMLDivElement>[] = [];

  const positionRef = useRef<number>(0);

  const [carouselSizes, setCarouselSizes] = useState<CarouselSizes>(
    carouselSizesRef.current
  );
  const [wrapperStyles, setwrapperStyles] = useState<CSSProperties>(
    style || {}
  );

  const resizeModule = useRef<Resize>();
  const pointersControlModule = useRef<PointerControls>();
  const motionModule = useRef<Motion>();
  const focusControlsModule = useRef<FocusControls>();
  const tabControlsModule = useRef<TabControls>();

  /**
   * Sets main track position
   * TODO: Add aditional handler fo
   */
  const setPosition = useCallback(() => {
    if (!track.current) return;

    let position = 0;
    let delta = 0;

    for (let key in carouselPosition.current) {
      position += carouselPosition.current[key].x;
      delta += carouselPosition.current[key].delta;
    }

    if (mode === "continuous" && carouselSizesRef.current.containerWidth > 0) {
      while (position < -carouselSizesRef.current.containerWidth) {
        position += carouselSizesRef.current.containerWidth;
      }
      while (position > carouselSizesRef.current.leftShift) {
        position -= carouselSizesRef.current.containerWidth;
      }
    }

    positionRef.current = position;
    track.current.style.transform = `translateX(${position}px) translateZ(0px)`;
  }, []);

  /**
   * Resize handler. Handles track size, children sizes and the number of fake elements, needed for a contionius carousel to work
   */
  const handleResize = useCallback(() => {
    if (!resizeModule.current) return;

    const sizes = resizeModule.current.resize();

    if (sizes) {
      carouselSizesRef.current = {
        containerWidth: sizes.containerWidth,
        gapWidth: sizes.gapWidth,
        itemWidth: sizes.itemWidth,
        totalWidth: sizes.totalWidth,
        leftShift: sizes.leftShift,
        numberOfFakeElements: sizes.numberOfFakeElements,
      };

      setCarouselSizes(carouselSizesRef.current);

      carouselPosition.current.modeShift.x = sizes.leftShift;

      setPosition();
    }
  }, []);

  /**
   * Motion handler. Fires when positon of the track changes for whatever reason
   */
  const handleMotion = useCallback<MotionCallback>((motionData) => {
    if (
      Math.abs(carouselPosition.current.motion.x - motionData.position) >
        0.05 ||
      Math.abs(carouselPosition.current.motion.delta - motionData.delta)
    ) {
      carouselPosition.current.motion.x = motionData.position;
      carouselPosition.current.motion.delta = motionData.delta;

      setPosition();
    }
  }, []);

  /**
   * Drag & Touch handler
   */
  const handlePointer = useCallback<PointerCallback>(
    ({ dragging, pointerX, touchAction }) => {
      if (!pointersControlModule.current) return;

      if (dragging) {
        setwrapperStyles((prevStyles) => {
          return prevStyles.cursor === "grabbing" &&
            prevStyles.touchAction === touchAction
            ? prevStyles
            : {
                ...prevStyles,
                cursor: "grabbing",
                touchAction: touchAction ? touchAction : prevStyles.touchAction,
              };
        });
      } else {
        setwrapperStyles((prevStyles) => {
          return prevStyles.cursor === "grab" &&
            prevStyles.touchAction === touchAction
            ? prevStyles
            : {
                ...prevStyles,
                cursor: "grab",
                touchAction: touchAction ? touchAction : prevStyles.touchAction,
              };
        });
      }

      if (motionModule.current) {
        motionModule.current.applyDrag({
          isDragActive: dragging,
          dragPosition: pointerX || 0,
        });
      }
    },
    []
  );

  /**
   * Carousel event handler
   */
  const handleEvent = useCallback((event: CarouselEvent) => {
    if (!event || event.payload.id !== id || !motionModule.current) {
      return;
    }

    const position =
      Math.min(0, Math.max(-(carouselSizesRef.current.totalWidth - carouselSizesRef.current.containerWidth), event.type === "carousel:to"
        ? -event.payload.scrollValue
        : event.type === "carousel:toIndex"
        ? -event.payload.scrollValue *
          (carouselSizesRef.current.itemWidth +
            carouselSizesRef.current.gapWidth)
        : event.type === "carousel:toPercent"
        ? -event.payload.scrollValue *
          (carouselSizesRef.current.totalWidth -
            carouselSizesRef.current.containerWidth)
        : event.type === "carousel:toVw"
        ? -event.payload.scrollValue * syncResize.get().width
        : event.type === "carousel:by"
        ? positionRef.current - event.payload.scrollValue
        : event.type === "carousel:byIndex"
        ? positionRef.current -
          event.payload.scrollValue *
            (carouselSizesRef.current.itemWidth +
              carouselSizesRef.current.gapWidth)
        : event.type === "carousel:byPercent"
        ? positionRef.current -
          event.payload.scrollValue *
            (carouselSizesRef.current.totalWidth -
              carouselSizesRef.current.containerWidth)
        : event.type === "carousel:byVw"
        ? positionRef.current -
          event.payload.scrollValue * syncResize.get().width
        : 0));

    if (Math.abs(position - positionRef.current) < 0.1) {
      return;
    }
    
    motionModule.current.applyTransition({
      to: position
    });
  }, []);

  useEffect(() => {
    carouselEventEmitter.subscribe(handleEvent);
    syncResize.subscribe(handleResize);

    return () => {
      carouselEventEmitter.unsubscribe(handleEvent);
      syncResize.unsubscribe(handleResize);
    };
  }, []);

  /**
   * Resize module
   */
  useEffect(() => {
    if (track.current && wrapper.current) {
      const options = {
        mode,
        resizeElements,
        elementsPerScreen: elementsPerScreenRef.current,
        gap,
        itemWidth,
        padding,
        paddingUnits,
        maximumItemWidth,
        total,
        container: track.current,
        outerWrapper: wrapper.current,
      };
      if (mode === "continuous") {
        resizeModule.current = new ResizeContinuous(options);
      } else if (mode === "simple") {
        resizeModule.current = new ResizeSimple(options);
      }

      handleResize();
    }

    return () => {
      if (resizeModule.current) {
        resizeModule.current.destroy();
        resizeModule.current = undefined;
      }
    };
  }, [
    mode,
    resizeElements,
    elementsPerScreenRef.current,
    gap,
    itemWidth,
    padding,
    paddingUnits,
    maximumItemWidth,
    total,
    track.current,
    wrapper.current,
  ]);

  /**
   * Pointer controls module
   */
  useEffect(() => {
    if (interactive === "always" && wrapper.current) {
      pointersControlModule.current = new PointerControls({
        outerWrapper: wrapper.current,
        callback: handlePointer,
      });

      setwrapperStyles((prevStyles) => {
        return {
          ...prevStyles,
          cursor: "grab",
          userSelect: "none",
          MozUserSelect: "none",
          WebkitTouchCallout: "none",
          touchAction: "pan-y",
        };
      });
    }

    return () => {
      if (pointersControlModule.current) {
        pointersControlModule.current.destroy();
        pointersControlModule.current = undefined;

        setwrapperStyles((prevStyles) => {
          return {
            ...prevStyles,
            cursor: "auto",
            userSelect: "auto",
            MozUserSelect: "auto",
            WebkitTouchCallout: "default",
            touchAction: "auto",
          };
        });
      }
    };
  }, [interactive, wrapper.current]);

  /**
   * Focus module
   */
  useEffect(() => {
    if (interactive === "always" && wrapper.current) {
      focusControlsModule.current = new FocusControls({
        id,
        container: wrapper.current
      });
    }
    return () => {
      if (focusControlsModule.current) {
        focusControlsModule.current.destroy();
        focusControlsModule.current = undefined;
      }
    }
  }, [id, interactive, wrapper.current]);

  /**
   * Motion module
   */
  useEffect(() => {
    motionModule.current = new Motion({
      position: carouselPosition.current.motion.x,
      mode,
      itemWidth: carouselSizesRef.current.itemWidth,
      gapWidth: carouselSizesRef.current.gapWidth,
      totalWidth: carouselSizesRef.current.totalWidth,
      containerWidth: carouselSizesRef.current.containerWidth,
      inertionCorrection,
      inertionDecay,
      inertionInterpolationFunction,
      motionCallback: handleMotion,
    });

    return () => {
      if (motionModule.current) {
        motionModule.current.destroy();
      }
    };
  }, [
    mode,
    carouselSizesRef.current.itemWidth,
    carouselSizesRef.current.gapWidth,
    carouselSizesRef.current.totalWidth,
    carouselSizesRef.current.containerWidth,
    inertionCorrection,
    inertionDecay,
    inertionInterpolationFunction,
  ]);

  itemRefs = [];

  let items = children.map((ch) => {
    const ref = React.createRef<HTMLDivElement>();
    itemRefs.push(ref);

    const style: React.CSSProperties = {
      ...ch.props.style,
      width: carouselSizes.itemWidth,
      marginRight: carouselSizes.gapWidth,
    };

    return (
      <CarouselItem
        ref={ref}
        fake={false}
        className={ch.props.className}
        style={style}
        key={ch.key}
      >
        {ch.props.children}
      </CarouselItem>
    );
  });

  /**
   * Tab Module
   */
  useEffect(() => {
    if (interactive === "always" && wrapper.current) {
      tabControlsModule.current = new TabControls({
        id,
        container: wrapper.current,
        itemRefs,
        applySectionScrollFix
      });
    }
    return () => {
      if (tabControlsModule.current) {
        tabControlsModule.current.destroy();
        tabControlsModule.current = undefined;
      }
    }
  });

  const fakeItemsBefore: JSX.Element[] = [];
  const fakeItemsAfter: JSX.Element[] = [];

  for (let i = 0; i < carouselSizes.numberOfFakeElements; i++) {
    const j = i % total;

    fakeItemsBefore.unshift(
      React.cloneElement(items[total - j - 1], {
        fake: true,
        key: `fake-1-${items[j].key}`,
      })
    );

    fakeItemsAfter.push(
      React.cloneElement(items[j], {
        fake: true,
        key: `fake-0-${items[j].key}`,
      })
    );
  }

  Array.prototype.unshift.apply(items, fakeItemsBefore);
  items = items.concat(fakeItemsAfter);

  const containerStyles: React.CSSProperties = {
    display: "flex",
    flexWrap: "nowrap",
    width: carouselSizes.totalWidth ? carouselSizes.totalWidth : "100%",
  };

  return (
    <div ref={wrapper} className={className} style={wrapperStyles}>
      <div ref={track} style={containerStyles}>
        {items}
      </div>
    </div>
  );
};
