import { syncResize } from "../../../resize";
import type { Resize, ResizeOptions } from "./interface";

export class ResizeContinuous implements Resize {
  private gap;
  private outerWrapper;
  private container;
  private elementsPerScreen;
  private resizeElements;
  private padding;
  private paddingUnits;
  private itemWidth;
  private maximumItemWidth;
  private total;

  constructor(options: ResizeOptions) {
    this.gap = options.gap;
    this.outerWrapper = options.outerWrapper;
    this.container = options.container;
    this.elementsPerScreen = options.elementsPerScreen;
    this.resizeElements = options.resizeElements;
    this.padding = options.padding;
    this.paddingUnits = options.paddingUnits;
    this.itemWidth = options.itemWidth;
    this.maximumItemWidth = options.maximumItemWidth;
    this.total = options.total;
  }

  destroy = () => {};

  resize = () => {
    if (!this.container) return;

    const screenWidth = syncResize.get().width;
    let itemsPerWrapper;

    if (this.resizeElements) {
      itemsPerWrapper = this.elementsPerScreen.reduce(function (prev, cur) {
        return cur.breakpoint < screenWidth ? cur : prev;
      }).number;

      this.itemWidth = Math.floor(
        Math.min(
          this.maximumItemWidth,
          (this.outerWrapper.clientWidth - this.gap * (itemsPerWrapper - 1)) /
            itemsPerWrapper
        )
      );
    }

    const padding =
      this.paddingUnits === "px"
        ? this.padding
        : this.paddingUnits === "%"
        ? (this.container.clientWidth * this.padding) / 100
        : this.paddingUnits === "vw"
        ? screenWidth * this.padding
        : 0

    itemsPerWrapper =
      Math.floor(
        (this.outerWrapper.clientWidth + padding) /
          (this.itemWidth + this.gap)
      ) + 1;

    const leftShift = -itemsPerWrapper * (this.itemWidth + this.gap);
    const totalWidth = (this.itemWidth + this.gap) * this.total;
    const numberOfFakeElements = itemsPerWrapper;

    return {
      containerWidth: this.container.clientWidth,
      itemWidth: this.itemWidth,
      gapWidth: this.gap,
      totalWidth,
      leftShift,
      numberOfFakeElements,
    };
  };
}
