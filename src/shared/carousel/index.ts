import { Carousel } from "./CarouselMain";
import { CarouselControl } from "./CarouselControl";

export {
  Carousel,
  CarouselControl
}