const timeBasedAnimations = false;
const asyncMasterLoop = false;

const exports = {
    timeBasedAnimations,
    asyncMasterLoop
}

export default exports;